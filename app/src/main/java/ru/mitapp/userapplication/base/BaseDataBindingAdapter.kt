package ru.mitapp.userapplication.base

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import ru.mitapp.userapplication.R

    @BindingAdapter("imageUrl")
    fun loadImage(imageView: ImageView, url: String?) {
        Glide.with(imageView.context).load(url).placeholder(R.drawable.ic_placeholder).into(imageView)
    }
