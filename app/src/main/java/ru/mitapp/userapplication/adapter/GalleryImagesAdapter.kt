package ru.mitapp.userapplication.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import ru.mitapp.userapplication.R
import ru.mitapp.userapplication.databinding.ItemGalleryImageBinding
import ru.mitapp.userapplication.extension.loadImage
import ru.mitapp.userapplication.model.GalleryImage

class GalleryImagesAdapter(
    var list: MutableList<GalleryImage>,
    var selectImages: ArrayList<String>,
) :
    RecyclerView.Adapter<GalleryImagesAdapter.GalleryImagesViewHolder>() {

    var context: Context? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GalleryImagesViewHolder {
        val binding: ItemGalleryImageBinding? = DataBindingUtil.bind(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_gallery_image, parent, false)
        )

        context = parent.context

        return GalleryImagesViewHolder(binding!!)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: GalleryImagesViewHolder, position: Int) {
        val image = list[position]
        holder.binding.image.loadImage(holder.itemView.context, image.imagePath)

//        for (imagePath in selectImages) {
//            if (imagePath == image.imagePath)
//                image.isSelected = true
//        }

        holder.binding.itemView.setOnClickListener {

        }
    }

    class GalleryImagesViewHolder(
        var binding: ItemGalleryImageBinding
    ) :
        RecyclerView.ViewHolder(binding.root)

    interface GalleryListener {
        fun onClickItem(galleryImage: GalleryImage)
    }
}