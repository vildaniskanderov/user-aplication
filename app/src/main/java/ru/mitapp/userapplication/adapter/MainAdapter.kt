package ru.mitapp.userapplication.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import ru.mitapp.userapplication.R
import ru.mitapp.userapplication.databinding.CompanyItemBinding
import ru.mitapp.userapplication.model.Company

class MainAdapter(var items: ArrayList<Company>, var listener: Listener) :
    RecyclerView.Adapter<MainAdapter.MainViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainViewHolder {
        val binding: CompanyItemBinding? = DataBindingUtil.bind(
            LayoutInflater.from(parent.context).inflate(
                R.layout.company_item, parent, false
            )
        )
        return MainViewHolder(binding!!)
    }

    override fun onBindViewHolder(holder: MainViewHolder, position: Int) {
        holder.onBind(items[position])
    }

    override fun getItemCount(): Int {
        return items.size
    }

    class MainViewHolder(var binding: CompanyItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun onBind(company: Company) {
            binding.company = company
        }
    }

    interface Listener{

    }
}