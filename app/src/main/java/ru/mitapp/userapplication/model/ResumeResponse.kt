package ru.mitapp.userapplication.model

data class ResumeResponse(
    var user: User? = null
)