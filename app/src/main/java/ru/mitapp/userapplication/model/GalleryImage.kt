package ru.mitapp.userapplication.model

data class GalleryImage(
    var imagePath : String? = null,
    var isSelected : Boolean? = null

)