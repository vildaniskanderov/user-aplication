package ru.mitapp.userapplication.model

import java.io.Serializable

data class Company(
    var name: String? = null,
    var position: String? = null
) : Serializable