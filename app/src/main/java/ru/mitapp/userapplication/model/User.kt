package ru.mitapp.userapplication.model

import java.io.Serializable

data class User(
    var first_name: String? = null,
    var photo: String? = null,
    var second_name: String? = null,
    var education: Int? = null,
    var companies: ArrayList<Company> = ArrayList()
) : Serializable