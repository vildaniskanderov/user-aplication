package ru.mitapp.userapplication.activity

import android.content.Intent
import android.text.Editable
import android.text.TextWatcher
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import ru.mitapp.userapplication.R
import ru.mitapp.userapplication.adapter.GalleryImagesAdapter
import ru.mitapp.userapplication.adapter.MainAdapter
import ru.mitapp.userapplication.base.BaseActivity
import ru.mitapp.userapplication.databinding.ActivityMainBinding
import ru.mitapp.userapplication.extension.showToast
import ru.mitapp.userapplication.model.Company
import ru.mitapp.userapplication.model.User

class MainActivity : BaseActivity<ActivityMainBinding>(R.layout.activity_main),
    MainAdapter.Listener {

    private var back = false
    var user : User = User()
    private var company: ArrayList<Company> = ArrayList()

    override fun setupView() {
        fillUser()
        setupRecycler(company)

        binding!!.educationName.text = educationName(user.education)

        binding!!.userImage.setOnClickListener{
            PhotoActivity.onStart(this, user.photo, binding!!.userImage)
        }

        binding!!.button.setOnClickListener {
            val intent = Intent(this,GalleryActivity::class.java)
            startActivity(intent)
        }

        binding!!.educationInput.addTextChangedListener(object : TextWatcher{
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable?) {
                if (!s.isNullOrEmpty())
                binding!!.educationName.text = educationName(s.toString().toInt())
                else binding!!.educationName.text = educationName(null)
            }
        })
    }

    private fun fillUser(){
        user.first_name = "Vildan"
        user.second_name = "Iskanderov"
        user.education = 2
        user.photo = "https://avatars.mds.yandex.net/get-kinopoisk-image/1777765/7d4eb861-1f80-44da-b75e-7f365d0fc3da/360"

       user.companies.add(Company("Google", "Android developer"))
       user.companies.add(Company("Facebook", "Android developer"))
       user.companies.add(Company("General Motors", "Android developer"))

        binding!!.user = user
    }

    private fun educationName(education : Int?) : String{
        return  when(education){
            1 -> "High school"
            2 -> "Bachelor"
            3 -> "Master"
            4 -> "Doctoral"
            else -> getString(R.string.education)
        }
    }

    private fun setupRecycler(company: ArrayList<Company>) {
        if (company.isNullOrEmpty()) {
            binding!!.companyRecycler.adapter = MainAdapter(user.companies, this)
        }
    }

    override fun onBackPressed() {
        if (back) {
            super.onBackPressed()
        } else {
            back = true
            showToast(getString(R.string.back_toast_message))
            GlobalScope.launch {
                delay(2000L)
                back = false
            }
        }
    }
}