package ru.mitapp.userapplication.activity

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Gallery
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.GridLayoutManager
import kotlinx.android.synthetic.main.item_gallery_image.*
import kotlinx.android.synthetic.main.item_gallery_image.view.*
import ru.mitapp.userapplication.R
import ru.mitapp.userapplication.adapter.GalleryImagesAdapter
import ru.mitapp.userapplication.databinding.ActivityGalleryBinding
import ru.mitapp.userapplication.extension.showToast
import ru.mitapp.userapplication.model.GalleryImage
import ru.mitapp.userapplication.model.User
import ru.mitapp.userapplication.utils.ImagesGallery

class GalleryActivity : AppCompatActivity(), GalleryImagesAdapter.GalleryListener {

    private val PERMISSION_REQUEST_CODE = 1

    lateinit var binding: ActivityGalleryBinding
    private var images: ArrayList<GalleryImage> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_gallery)

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
            != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                PERMISSION_REQUEST_CODE
            )
        } else {
            loadImages()
        }

        binding.closeButton.setOnClickListener {
            onBackPressed()
        }
    }

    private fun loadImages() {
        binding.galleryRecycler.setHasFixedSize(true)
        images = ImagesGallery.listOfImages(this)
        binding.galleryRecycler.adapter = GalleryImagesAdapter(images, selectImages )
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == PERMISSION_REQUEST_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                loadImages()
            } else {
                showToast("")
            }
        }
    }

    companion object {
        var ACTIVITY_CODE = 10
        var selectImages = ArrayList<String>()
        var isSingleClick: Boolean = false
        fun start(
            context: Activity,
            imagesPath: ArrayList<String>,
            isSingleClick: Boolean = false
        ) {
            val intent = Intent(context, GalleryActivity::class.java)
            this.selectImages = imagesPath
            this.isSingleClick = isSingleClick
            context.startActivityForResult(intent, ACTIVITY_CODE)
        }
    }

    override fun onClickItem(galleryImage: GalleryImage) {

    }
}