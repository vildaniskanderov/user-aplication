package ru.mitapp.userapplication.activity

import android.app.Activity
import android.content.Intent
import android.view.WindowManager
import android.widget.ImageView
import android.widget.Toast
import androidx.core.app.ActivityOptionsCompat
import androidx.core.view.ViewCompat
import com.bumptech.glide.Glide
import ru.mitapp.userapplication.R
import ru.mitapp.userapplication.base.BaseActivity
import ru.mitapp.userapplication.databinding.ActivityPhotoBinding


const val IMAGE_URL = "image_url"

class PhotoActivity : BaseActivity<ActivityPhotoBinding>(R.layout.activity_photo) {

    private val imageUrl : String?
    get() = intent.getStringExtra(IMAGE_URL)

    override fun setupView() {
        window.setFlags(
            WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
            WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS
        )

        Glide.with(this).load(imageUrl).into(binding!!.photoView)
        binding!!.closeButton.setOnClickListener{
            onBackPressed()
        }

    }


    companion object {
        fun onStart(activity: Activity, imageUrl : String?, imageView: ImageView? = null) {
            if (!imageUrl.isNullOrEmpty()){
                val intent = Intent(activity, PhotoActivity::class.java)
                intent.putExtra(IMAGE_URL, imageUrl)
                if (imageView != null){
                    val options: ActivityOptionsCompat = ActivityOptionsCompat.makeSceneTransitionAnimation(
                        activity, imageView,
                        ViewCompat.getTransitionName(imageView)!!
                    )
                    activity.startActivity(intent, options.toBundle())
                } else{
                    activity.startActivity(intent)
                }
            }else{
                Toast.makeText(activity, "Фото не найдено", Toast.LENGTH_LONG).show()
            }


        }
    }

}